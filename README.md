tunnel-racer
===========

Simple game where you have to avoid obstacles whilst your speed increases. Control by rotating and tilting your phone.

![Screenshot](Screenshot.png)
